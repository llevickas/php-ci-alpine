FROM php:7.4-cli-alpine

RUN apk --update add \
		freetype-dev \
		gd-dev \
		jpeg-dev \
		libjpeg-turbo-dev \
		libpng-dev \
		libxml2-dev \
		libxml2-utils \
		libzip-dev \
		oniguruma-dev \
		zlib-dev \
		autoconf \
		curl \
		g++ \
		git \
		lftp \
		make \
		openssh \
		openssh-client \
		rsync \
		yarn \
		jq

RUN docker-php-ext-install mbstring
RUN docker-php-ext-install zip
RUN docker-php-ext-install opcache
RUN docker-php-ext-install pdo
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install soap
RUN docker-php-ext-install fileinfo
RUN docker-php-ext-install mysqli
RUN docker-php-ext-install gd

RUN echo "memory_limit = -1" >> /usr/local/etc/php/php.ini

RUN pecl install pcov
RUN echo "extension=pcov.so" > /usr/local/etc/php/conf.d/pcov.ini
RUN echo "pcov.enabled = 1"  > /usr/local/etc/php/conf.d/pcov.ini

RUN pecl install && pecl install xdebug-2.8.1 && docker-php-ext-enable xdebug

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer

RUN mkdir -p /var/www
WORKDIR /var/www
COPY . /var/www
VOLUME /var/www

RUN mkdir -p ~/.ssh
RUN printf 'Host *\n\tStrictHostKeyChecking no' > ~/.ssh/config
RUN chmod 400 ~/.ssh/config

CMD ["/bin/sh"]

ENTRYPOINT ["/bin/sh", "-c"]
